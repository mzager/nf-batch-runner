# Adding a new pipeline
After creating an environment with a web front end browse to the cloudfront URL given and login.
![UI login](images/ui-login.png "Login page")

When you first login you will see a message next to a cog which is the place to click to add a new pipeline
![UI add pipeline cog](images/ui-click-cog.png "Add a new pipeline")

Give the pipeline a name and upload a YAML configuration file (it must have the .yaml suffix) that describes the pipeline

![UI upload YAML](images/ui-upload-yaml.png "Upload Yaml manifest")

The YAML configuration has 4 compulsory elements:

1. command  
This is the path to the Nextflow workflow file (usually ending .nf) e.g main.nf
If you follow the template below for building the contain to run Nextflow then the WORKDIR for the container will be the directory containing the nf file and so the this can be just the name of Nextflow file.

2. pipelineContainer  
Specify the name of a public docker container that has the necessary code to run the Nextflow container. We suggest building it from the latest Nextflow docker container (e.g nextflow/nextflow:19.07.0), cloning your Nextflow pipeline repository to a directory that you set as the WORKDIR.
We give examples [here](../examples/simple_assembly/runner.Dockerfile) and  [here](../examples/simple_mapping/runner.Dockerfile)

3. pipelineDependencies  
A container that provides the software to be used by the Nextflow pipeline.

4. parameters
This section is a list of the parameters that will be passed to the `nextflow run` command as well as the parameter type that determined the way they will be represented in the web UI. The basic format is  
    ```
    name: <PARAM NAME>
    label: <LABEL IN WEB UI>
    type: <ONE OF file, string, number, boolean, list or map>
    required: true
    default: <DEFAULT VALUE>
    ```
    the name will be translated to a Nextflow param e.g `name: input_dir` will be translated to `-- input_dir` in the command line that Nextflow will run.  
    The types are interpreted in the web UI as follows:  
    - `file` will be rendered as a text field and expects a `s3://` prefix
    - `string` will be rendered as a text field and validation will check it is text 
    - `number` will be rendered as a text field and validation will check it is a number 
    - `boolean` will be rendered as a drop down with either yes or no. If yes is selected the parameter will be added to the command line as `-- <PARAM NAME>` otherwise if no is selected the parameter will not be included on the command line.
    - `list` will be rendered as a drop down and the value passed to the command line will be the same as displayed in the drop down.
    - `map` will be rendered as a drop down where the map keys are displayed in the dropdown and the value passed to the command line will be the map values.  

    Examples of yaml files are [here](../examples/simple_assembly/workflow_params.yaml) and [here](../examples/simple_mapping/workflow_params.yaml)
