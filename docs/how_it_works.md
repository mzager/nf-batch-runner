# How it works

This tool deploys infrastucture in your AWS account using a CloudFormation template.  This is done using a Python package called `nf-batch-runner`.  We 
also deploy an optional frontend which lets you run pipelines from your browser.

## Infrastructure

Your pipeline is run in your AWS account using the AWS Batch functionality built into Nextflow.  The Nextflow process runs in a container using AWS ECS Fargate.
That process is started using an AWS Lambda function which is monitoring a DynamoDB database for new jobs.

Progress updates are reported using an API Gateway and another AWS Lambda.  These are recorded in another DynamoDB database and the pipeline record is also updated.

All inputs, outputs, and the working directory are stored in a 'FilesBucket' which is a S3 bucket created by this tool.  Logs are also stored in this bucket as 
processes complete.

## Web interface

The web interface is a combination of a single page static site built using Vue / Vuex.  This is stored in an S3 bucket.

The API is built using a combination of API Gateway and a Lambda function.

A CloudFront distribution sits in front of the static content and the API and makes it available on a secure endpoint using https.

The web interface stores hashed and salted passwords in the User database created by the Infrastructure.

## CLI

The CLI is written in Python.  When you create a new environment, it pulls the latest CloudFormation template from our AWS account.  It then asks you 
a series of questions which it uses to fill out the paramaters in the CloudFormation template.

While the stack is being created, it makes a copy of the frontend into your account.  This limits your dependency on us once the pipeline is installed.

## Dependencies

The only dependency on our infrastucture is an AMI we have built and which is pulled from our AWS account.  It is simple for you to start using your own 
if necessary (but this is much more convenient).