# Debugging

Your environment is created using AWS CloudFormation.  You can see the [resources it created](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2) in the Amazon Console.  Note that, by default, we create resources in the London region - `eu-west-2`.

When you created your stack, you gave it a name like `my-environment`.  Click on that stack and then click on "Outputs".  You will see a table like this:

Key | Value | Description | Export name
FilesBucket | bt5-cli-test-filesbucket-4t1whgy2vc16 | S3 bucket used for pipeline inputs and outputs | bt5-cli-test-FilesBucket
FrontendBucket | bt5-cli-test-frontendbucket-19yp4d9dfiw0 | S3 bucket for the frontend | -
FrontendUrl | d1q83rcyz9fxwf.cloudfront.net | URL for frontend | -
PipelinesDb | bt5-cli-test-PipelinesDb-7U6SDB27HVFO | Database containing pipelines | bt5-cli-test-PipelinesDb
PipelinesDbStream | arn:aws:dynamodb:eu-west-2:012345678910:table/bt5-cli-test-PipelinesDb-7U6SDB27HVFO/stream/2019-09-18T10:00:51.384 | Pipelines database event 	| bt5-cli-test-PipelinesDbStream
UsersDb | bt5-cli-test-UsersDb-1GKUCRGEEDDZV | Database containing pipeline users | bt5-cli-test-UsersDb

`FrontendUrl` tells you where you can log into the frontend.
`FilesBucket` tells you where you can store input and output files.

## Where are the logs

Each process creates logs which we store in the `logs` directory of the `FilesBucket`.

*More todo*