const USER = {
  user: 'abc',
  credits: {
    balance: 10,
    lastDebited: 1234, // seconds since epoc
    rate: 0.001, // how many credits are added per seconds
    maxBalance: 100
  },
  metadata: {} // service specific metadata
}

const PIPELINE = {
  pipeline: 'abc',
  user: 'abc',
  status: 'PENDING|STARTING|RUNNING|SUCCESS|FAILED',
  created: 1234, // seconds since epoc
  started: 1234, // seconds since epoc
  inputs: {
    status: "PENDING|VALID|INVALID|READY",
    args: ["--foo", "bar"],
    pipelineContainer: "foobar:latest",
    pipelineDependencies: "foobar:latest",
    variables: {},
    hash: "deduplication hash",
    memory: 1024, //megabytes
    timeout: 3600 // seconds
  },
  charges: {
    status: "PENDING|READY|PAID|OUT_OF_CREDIT",
    cost: 1,
    updated: 1234 // seconds since epoc
  },
  metadata: {}
}

const PROGRESS = {
  pipeline: String,
  status: String,
  runId: String,
  started: 1234, // seconds since epoc
  finished: 1234, // seconds since epoc
  events: []
}

const HISTORY = {
  hash: 'deduplication hash',
  pipelines: [String]
}