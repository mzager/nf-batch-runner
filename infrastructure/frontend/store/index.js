import { signin, signout } from './helpers/api';

export const state = () => ({
  dark: false,
  error: false,
  username: '',
  password: '',
  language: undefined,
});

export const getters = {
  themeIcon: (state) => (state.dark) ? 'mdi-weather-night' : 'mdi-white-balance-sunny',
};

export const mutations = {
  setUsername(state, value) {
    state.username = value;
  },
  setPassword(state, value) {
    state.password = value;
  },
  setDark(state, value) {
    state.dark = value;
  },
  setError(state, value) {
    state.error = value;
  },
  setLanguage(state, value) {
    state.language = value;
  },
};

export const actions = {
  async signin({ state, commit }) {
    try {
      const { username, password } = state;
      await signin({ username, password });
      commit('setError', false);
      this.$router.push('/dashboard');
    } catch (exception) {
      commit('setError', true);
    }
  },
  async signout({ commit }) {
    await signout();
    this.$router.push('/');
  },
};
