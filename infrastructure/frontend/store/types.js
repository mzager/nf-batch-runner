export const state = () => ({
  all: [
    {
      name: 'boolean',
      assignable: true,
      default: false,
    },
    {
      name: 'integer',
      assignable: true,
      default: 0,
    },
    {
      name: 'number',
      assignable: true,
      default: 0,
    },
    {
      name: 'text',
      assignable: true,
      default: '',
    },
    {
      name: 'file',
      assignable: true,
      default: '',
    },
    {
      name: 'dir',
      assignable: true,
      default: '',
    },
    {
      name: 'list',
      assignable: true,
      default: '',
    },
    {
      name: 'map',
      assignable: true,
      default: '',
    },
  ],
});

export const getters = {
  byName(state) {
    const dict = {};
    for (const item of state.all) {
      dict[item.name] = item;
    }
    return dict;
  },
  names(state) {
    return state.all.map((item) => item.name);
  },
};
