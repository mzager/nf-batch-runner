import { listBuckets, listObjects } from '../helpers/api';

export const state = () => ({
  bucketSet: [],
  prefixSet: [],
  objectSet: [],
  selectedBucket: undefined,
  prefixes: [],
});

export const getters = {
  getBreadcrumb: ({ prefixes }) => prefixes.map((text, index) => ({
    text,
    index,
  })),
  getObjectStore(state) {
    return state.bucketSet.map((bucket) => ({
      ...bucket,
      Prefixes: state.prefixSet
        .filter((prefix) => bucket.Prefixes.includes(prefix.Key))
        .map((prefix) => ({
          ...prefix,
          Objects: state.objectSet
            .filter((object) => prefix.Objects.includes(object.Key)),
        })),
    }));
  },
  getBucket: (state) => state.bucketSet.find(({ Key }) => Key === state.selectedBucket) || {},
  getBucketPopulated: (state, getters) => {
    const bucket = getters.getBucket;
    const { Prefixes = [] } = bucket;
    return ({
      ...bucket,
      Prefixes: state.prefixSet
        .filter((prefix) => Prefixes.includes(prefix.Key))
        .map((prefix) => ({
          ...prefix,
          Objects: state.objectSet
            .filter((object) => prefix.Objects.includes(object.Key)),
        })),
    });
  },
  getBucketPrefixesPopulated: (state, getters) => {
    const { prefixes, prefixSet } = state;
    const bucket = getters.getBucket;
    const { Prefixes = [] } = bucket;
    if (Prefixes.length === 0) {
      return [];
    }
    console.log(Prefixes);
    return prefixes
      .reduce((acc, crumb, index) => {
        const key = (index === 0) ? '' : crumb;
        const foundIndex = prefixSet.findIndex((prefix) => (
          prefix.Position === index &&
          prefix.Key.replace('/', '') === key
        ));
        const prefix = prefixSet[foundIndex];
        if (typeof prefix !== 'undefined' && Prefixes.includes(prefix.Key)) {
          acc.push({
            ...prefix,
            Objects: state.objectSet
              .filter((object) => prefix.Objects.includes(object.Key)),
          });
        }
        return acc;
      }, []);
  },
  getPrefix: (state) => (K) => state.prefixSet.find(({ Key }) => Key === K),
  getObject: (state) => (K) => state.objectSet.find(({ Key }) => Key === K),
};

export const mutations = {
  addBucket(state, Key) {
    const index = state.bucketSet.findIndex((bucket) => bucket.Key === Key);
    if (index === -1) {
      state.bucketSet.push({
        Key,
        Prefixes: [],
      });
    } else {
      console.warn('There is already a Bucket with Key:', Key);
    }
  },
  removeBucket(state, Key) {
    // Buckets -< Prefixes -< Objects
    const index = state.bucketSet.findIndex((bucket) => bucket.Key === Key);
    if (index === -1) {
      console.warn('There is no Bucket with Key:', Key);
    } else {
      const { prefixes = [] } = state.bucketSet[index];
      for (const prefixKey of prefixes) {
        const prefixIndex = state.prefixSet.findIndex(({ Id }) => Id === prefixKey);
        if (prefixIndex !== -1) {
          const { objects = [] } = state.prefixSet[prefixIndex];
          for (const objectKey of objects) {
            const objectIndex = state.objectSet.findIndex(({ Id }) => Id === objectKey);
            if (objectIndex !== -1) {
              state.objectSet.splice(objectIndex, 1);
            }
          }
          state.prefixSet.splice(prefixIndex, 1);
        }
      }
      state.bucketSet.splice(index, 1);
    }
  },
  setObjects(state, data) {
    console.log('setObject', data);
    // Buckets -< Prefixes -< Objects
    const { bucketSet, prefixSet, objectSet } = state;
    const { Bucket, prefix, objects } = data;
    console.log({ Bucket, prefix, objects });
    const bucketIndex = bucketSet.findIndex(({ Key }) => Key === Bucket);
    if (bucketIndex === -1) {
      console.warn('Unable to find Bucket with Key:', Bucket);
      return;
    }
    const oldPrefixIndex = bucketSet[bucketIndex].Prefixes.indexOf(prefix.Key);
    // now we need to find all the old prefixSet objects
    // and remove their old objectSet objects
    if (oldPrefixIndex !== -1) {
      bucketSet[bucketIndex].Prefixes.splice(oldPrefixIndex, 1);
      const prefixIndex = prefixSet.findIndex(({ Key }) => Key === prefix.Key);
      if (prefixIndex === -1) {
        console.warn('Unable to find Prefix with Key:', prefix.Key);
      } else {
        const newObjectKeys = objects
          .map(({ Key }) => Key);
        console.log({ newObjectKeys });
        const curObjects = prefixSet[prefixIndex].Objects;
        console.log({ curObjects });
        const oldObjectKeys = curObjects.reduce((acc, Key) => {
          if (newObjectKeys.includes(Key)) {
            // push old objects to acc
            acc.push(Key);
          } else {
            // add the still "in date" objects Keys to the new prefix object.
            prefix.Objects.push(Key);
          }
          return acc;
        }, []);
        // now that we have found all the old objects we need to remove them
        for (const oldObjectKey of oldObjectKeys) {
          const objectIndex = objectSet.findIndex(({ Key }) => Key === oldObjectKey);
          if (objectIndex === -1) {
            console.warn('Unable to find Object with Key:', oldObjectKey);
          } else {
            objectSet.splice(objectIndex, 1);
          }
        }
        // we can now delete the old prefix as we no longer need it for reference
        prefixSet.splice(prefixIndex, 1);
      }
    }
    bucketSet[bucketIndex].Prefixes.push(prefix.Key);
    objects.forEach((object) => {
      objectSet.push(object);
      prefix.Objects.push(object.Key);
    });
    prefixSet.push(prefix);
  },
  setPath(state, { Prefix, Bucket }) {
    state.prefix = Prefix;
    state.bucket = Bucket;
  },
  setPrefixes(state, prefixes) {
    state.prefixes = prefixes;
  },
  setSelectedBucket(state, selectedBucket) {
    state.selectedBucket = selectedBucket;
  },
};

export const actions = {
  async listBuckets({ commit }) {
    try {
      const data = await listBuckets();
      const { buckets = [] } = data;
      buckets.forEach(({ Name }) => {
        commit('addBucket', Name);
      });
    } catch (e) {
      this.$router.push('/');
    }
  },
  async listObjects({ commit, state }, { params = {}, Position = 0 }) {
    const {
      Prefix,
      Bucket,
      ContinuationToken,
    } = params;
    commit('setSelectedBucket', Bucket);
    const crumbs = [ ...state.prefixes ];
    if (typeof Prefix !== 'undefined') {
      const crumb = Prefix.replace('/', '');
      const len = crumbs.length - 1;
      crumbs[Position] = crumb;
      const deleteCount = len - Position;
      if (deleteCount > 0) {
        crumbs.splice(Position + 1, deleteCount);
      }
      commit('setPrefixes', crumbs);
    }
    const prefixes = [ ...crumbs ];
    console.log(prefixes);

    const { prefixSet } = state;
    // stopping re requesting data
    if (typeof ContinuationToken === 'undefined') {
      console.log('req', {
        Prefix,
        Bucket,
      });
      prefixSet.forEach((pre) => {
        console.log('pre', pre);
        const b = (pre.Bucket === Bucket);
        const p = (typeof Prefix !== 'undefined' && pre.Key === Prefix);
        console.log(b, p);
      });
    }
    // params prefix is wrong
    const data = await listObjects({
      params,
      Position,
    });
    commit('setObjects', data);
  },
};
