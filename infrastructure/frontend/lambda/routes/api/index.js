const express = require('express');

const router = express.Router();

router.use('/pipeline', require('./pipeline'));
router.use('/job', require('./job'));
router.get('/listBuckets', require('./listBuckets'));
router.post('/listObjects', require('./listObjects'));

module.exports = router;
