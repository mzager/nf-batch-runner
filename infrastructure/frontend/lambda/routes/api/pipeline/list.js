const AWS = require('aws-sdk');

const { tables } = require('../../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = async(req, res, next) => {
  try {
    const { Item = {} } = await docClient.get({
      TableName: tables.users,
      Key: {
        user: req.session.user,
      },
    }).promise();
    const { configs = [] } = Item;
    res.json({ data: configs });
  } catch (e) {
    console.error('err', e);
    next();
  }
};
