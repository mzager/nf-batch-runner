const AWS = require('aws-sdk');
const uuid = require('uuidv4');

const { tables } = require('../../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = async(req, res, next) => {
  try {
    const {
      name,
      job,
      inputs,
      command,
    } = req.body;
    const args = [ command ].concat(
      inputs.reduce(
        (acc, { name, value = '', type, items = {} }) => {
          if (type === 'boolean') {
            if (value === true) acc.push(`--${name}`);
            return acc
          }
          if (type === 'map') {
            value = items[value] || '';
          }
          if (value !== '') {
            acc.push(`--${name}`);
            acc.push(`'${value}'`);
          }
          return acc;
        },
        [],
      )
    );
    const { Item = {} } = await docClient.get({
      TableName: tables.users,
      Key: {
        user: req.session.user,
      },
    }).promise();
    const { configs = [] } = Item;
    const config = configs.find((config) => config.name === name);
    if (typeof config === 'undefined') {
      throw new TypeError(`unable to find config with name: ${name}`);
    }
    const {
      cost = 1,
      hash,
      memory = 4000,
      pipelineContainer,
      pipelineDependencies,
      timeout = 3600,
    } = config;
    if (typeof pipelineContainer === 'undefined') {
      throw new TypeError('missing pipelineContainer');
    }
    const now = Date.now();
    const pipeline = uuid.fromString(now.toString());
    const doc = {
      charges: {
        cost,
        status: 'READY',
      },
      created: Math.round(now / 1000),
      inputs: {
        args,
        hash,
        memory,
        pipelineContainer,
        pipelineDependencies: pipelineDependencies || pipelineContainer,
        status: 'READY',
        timeout,
        variables: {
          inputs,
        },
      },
      metadata: {
        pipeline: name,
        job,
        created_at: now,
      },
      pipeline,
      status: 'PENDING',
      user: req.session.user,
    };
    await docClient.put({
      TableName: tables.jobs,
      Item: doc,
    }).promise();
    res.json({
      pipeline,
    });
  } catch (ex) {
    console.error(ex);
    next();
  }
};
