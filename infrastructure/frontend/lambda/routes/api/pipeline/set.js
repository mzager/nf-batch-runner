const AWS = require('aws-sdk');
const yaml = require('js-yaml');

const { tables } = require('../../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

const validate = (doc) => {
  switch (undefined) {
    case typeof doc.command:
      throw new TypeError('missing command');
    case typeof doc.pipelineContainer:
      throw new TypeError('missing pipelineContainer');
    case typeof doc.name:
      throw new TypeError('missing name');
    default:
      break;
  }
  if (!Array.isArray(doc.parameters)) {
    throw new TypeError('parameters must be an array');
  }
  doc.parameters.forEach((parameter, index) => {
    switch (undefined) {
      case parameter.name:
        throw new TypeError(`missing parameter name at position: ${index}`);
      case parameter.label:
        throw new TypeError(`missing parameter label at position: ${index}`);
      case parameter.type:
        throw new TypeError(`missing parameter type at position: ${index}`);
      default:
        break;
    }
  });
};

module.exports = async(req, res, next) => {
  try {
    const {
      name,
      config,
    } = req.body;
    switch ('undefined') {
      case typeof config:
        throw new TypeError('Missing config param');
      case typeof name:
        throw new TypeError('Missing name param');
    }
    const doc = yaml.safeLoad(config);
    validate(doc);
    await docClient.update({
      TableName: tables.users,
      Key: {
        user: req.session.user,
      },
      ReturnValues: 'ALL_NEW',
      UpdateExpression: 'set #configs = list_append(if_not_exists(#configs, :empty_list), :doc)',
      ExpressionAttributeNames: {
        '#configs': 'configs',
      },
      ExpressionAttributeValues: {
        ':doc': [{ name, ...doc }],
        ':empty_list': [],
      },
    }).promise();
    res.json({ data: doc });
  } catch (e) {
    console.error('err', e);
    next();
  }
};
