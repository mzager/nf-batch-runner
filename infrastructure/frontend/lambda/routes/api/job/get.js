const AWS = require('aws-sdk');

const { tables } = require('../../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = async(req, res, next) => {
  try {
    const { limit } = req.query;
    const { Items = [] } = await docClient.query({
      TableName: tables.jobs,
      IndexName: 'user-created-index',
      ConsistentRead: false,
      KeyConditionExpression: '#username = :username',
      ExpressionAttributeNames: {
        '#username': 'user',
      },
      ExpressionAttributeValues: {
        ':username': req.session.user,
      },
      Limit: limit,
    }).promise();
    res.json({ data: Items });
  } catch (e) {
    console.error('err', e.trace, e.code, e.stack);
    next();
  }
};
