const AWS = require('aws-sdk');

const s3 = new AWS.S3();

module.exports = async(req, res, next) => {
  try {
    const { Buckets = [] } = await s3.listBuckets().promise();
    res.json({ buckets: Buckets });
  } catch (exception) {
    console.error('err');
    next();
  }
};
