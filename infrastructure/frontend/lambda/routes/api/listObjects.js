const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const defaultParams = {
  FetchOwner: true,
  // setting the Delimiter to / makes the api treat prefixes like folders
  Delimiter: '/',
  Prefix: undefined,
  MaxKeys: 10,
};

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#listObjectsV2-property
module.exports = async(req, res, next) => {
  try {
    const { params = {}, Position } = req.body;
    // for the first request that isn't form a Bucket selection
    // this is to skip the object for the prefix itself
    const MaxKeys = (typeof params.ContinuationToken === 'undefined' && params.Prefix)
      ? defaultParams.MaxKeys + 1
      : defaultParams.MaxKeys;
    const payload = {
      ...defaultParams,
      ...params,
      MaxKeys,
    };
    const { Prefix = '', Bucket } = payload;
    const {
      Contents = [],
      CommonPrefixes = [],
      IsTruncated = false,
      ContinuationToken,
      NextContinuationToken,
    } = await s3.listObjectsV2(payload).promise();
    const Root = `~/${Bucket}/`;
    const Path = `${Root}${Prefix}`;
    res.json({
      Bucket,
      prefix: {
        Key: Prefix,
        Path,
        Fetched: Date.now().toString(),
        IsTruncated,
        ContinuationToken,
        NextContinuationToken,
        Position,
        Objects: [],
        Error: undefined,
      },
      objects: [
        ...Contents
          .filter((x) => x.Key !== Prefix)
          .map((x) => {
            const Key = x.Key.replace(Prefix, '');
            const Type = (Key.includes('.'))
              ? Key.split('.').pop()
              : Key;
            return {
              ...x,
              Key,
              Type,
            };
          }),
        ...CommonPrefixes.map((x) => ({
          Key: x.Prefix.replace(Prefix, ''),
          Type: 'dir',
        })),
      ],
    });
  } catch (exception) {
    console.error('err');
    next();
  }
};
