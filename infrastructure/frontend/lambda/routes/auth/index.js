const express = require('express');

const router = express.Router();

router.post('/signin', require('./signin'));
router.delete('/signout', require('./signout'));

module.exports = router;
