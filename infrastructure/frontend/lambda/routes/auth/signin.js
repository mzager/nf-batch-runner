const bcrypt = require('bcryptjs');
const AWS = require('aws-sdk');

const { tables } = require('../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = async(req, res, next) => {
  const { username = '', password = '' } = req.body;
  try {
    const { Item = {} } = await docClient.get({
      TableName: tables.users,
      Key: {
        user: username,
      },
    }).promise();
    if (typeof Item.password === 'undefined') {
      res.sendStatus(401);
    }
    const isValid = await bcrypt.compare(password, Item.password);
    if (!isValid) {
      res.sendStatus(401);
    } else {
      req.session.user = username;
      res.cookie('user', {});
      res.sendStatus(200);
    }
  } catch (exception) {
    console.error(exception);
    next();
  }
};
