const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const logger = require('morgan');
const connectDynamodb = require('connect-dynamodb');
const AWS = require('aws-sdk');
const { clearSession } = require('./utils');
const config = require('./utils/config');

AWS.config.update({
  region: config.aws.region,
});

const dev = config.node.env !== 'production';
const app = express();
const DynamoDBStore = connectDynamodb(session);
const bodyParserLimit = '500mb';

app.use(cookieParser());
app.use(bodyParser.json({ limit: bodyParserLimit }));
app.use(bodyParser.urlencoded({ extended: true, limit: bodyParserLimit }));
app.use(cors({ credentials: true, origin: true }));
app.use(logger('dev'));
if (!dev) {
  app.set('trust proxy', 1)
}
app.use(session({
  store: new DynamoDBStore({
    table: config.session.store,
    AWSRegion: config.aws.region,
  }),
  name: config.session.name,
  secret: config.session.secret,
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: (!dev),
  },
}));

const isAuthenticated = (req, res, next) => {
  if (!req.session.user) {
    // because our app is pretty simple we can just
    // sign them out if they don't have access rights
    clearSession(req, res);
    res.sendStatus(401);
  } else {
    next();
  }
};

app.use('/auth', require('./routes/auth'));
app.use('/api', isAuthenticated, require('./routes/api'));

if (require.main === module) {
  try {
    if (!config.node.port) {
      throw new Error('We need PORT set');
    }
    const port = parseInt(config.node.port);
    app.listen(port, (err) => {
      if (err) {
        throw err;
      }
      console.info(`API running on port: ${port}`);
    });
  } catch (ex) {
    console.error(ex.stack);
    process.exit(1);
  }
} else {
  const awsServerlessExpress = require('aws-serverless-express');
  const server = awsServerlessExpress.createServer(app);
  exports.handler = (event, context) => awsServerlessExpress.proxy(server, event, context, 'PROMISE').promise;
}
