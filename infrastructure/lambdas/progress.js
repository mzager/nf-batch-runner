const AWS = require("aws-sdk");
const { URL } = require("url");

const AWS_REGION = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION;
AWS.config.update({
  region: AWS_REGION
});

const s3 = new AWS.S3();

const { jitter, forObjects } = require('./utils')

// Statuses
const PENDING = "PENDING"
const RUNNING = "RUNNING"
const SUCCESS = "SUCCESS"
const FAILED = "FAILED"
const COMPLETED = "COMPLETED"

const PIPELINES_TABLE = process.env.PIPELINES_TABLE;
const PROGRESS_TABLE = process.env.PROGRESS_TABLE;
const NEXTFLOW_CLUSTER_ARN = process.env.NEXTFLOW_CLUSTER_ARN;
const NEXTFLOW_LOG_DIR = process.env.NEXTFLOW_LOG_DIR;
const MAX_RETRIES = 10;

function environmentVariablesOk() {
  switch (undefined) {
    case PIPELINES_TABLE:
      console.log({ error: "PIPELINES_TABLE is undefined" })
      return false;
    case PROGRESS_TABLE:
      console.log({ error: "PROGRESS_TABLE is undefined" })
      return false;
    case NEXTFLOW_CLUSTER_ARN:
      console.log({ error: "NEXTFLOW_CLUSTER_ARN is undefined" })
      return false;
    case NEXTFLOW_LOG_DIR:
        console.log({ error: "NEXTFLOW_LOG_DIR is undefined" })
        return false;
    default:
      return true
  }
}

const docClient = new AWS.DynamoDB.DocumentClient();

function res(statusCode, body) {
  return {
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode
  }
}

function validate(event) {
  const { path, body='{}', requestContext={} } = event;
  const { identity={} } = requestContext;
  const { sourceIp="missing" } = identity;

  const pathParts = path.split('/')
  if (pathParts.length !== 3) return { error: "Bad request", code: 1 }
  if (pathParts[0] !== "") return { error: "Bad request", code: 2 }
  if (pathParts[1] !== "nextflow") return { error: "Bad request", code: 3 }

  const pipeline = pathParts[2];
  return { pipeline, sourceIp, payload: JSON.parse(body)}
}

function now() {
  // Return current time in seconds
  return Math.floor(new Date() / 1000)
}

const MINUTES = 60
const HOURS = 60 * MINUTES
const DAYS = 24 * HOURS

async function appendProgress(pipeline, sourceIp, payload) {
  const { trace={}, runId, utcTime, event } = payload;
  const {
    task_id,
    status,
    exit,
    submit,
    start,
    process,
    tag,
    attempt,
    cpus,
    memory,
    complete,
    duration,
    realtime,
    "%cpu": cpu,
    read_bytes,
    write_bytes,
    vmem,
    rss,
    peak_vmem,
    peak_rss,
    native_id,
    workdir,
  } = trace;

  const doc = {
    pipeline,
    runId,
    utcTime,
    sourceIp,
    expires: now() + 90 * DAYS
  }
  if (status && exit !== undefined && exit <= 255) {
    let logDir = undefined;
    try {
      logDir = await backupLogs({ workdir, pipeline, process, status, tag, attempt })
    } catch (err) {
      console.log({ error: `Problem backing up logs for ${pipeline} from ${workdir}`, err })
    }
    Object.assign(doc, {
      task: `${status}|${process}|${tag}|${attempt}|${native_id}`,
      task_id,
      status,
      exit,
      submit,
      start,
      process,
      tag,
      attempt,
      "%cpus": cpus,
      memory,
      complete,
      duration,
      realtime,
      cpu,
      read_bytes,
      write_bytes,
      vmem,
      rss,
      peak_vmem,
      peak_rss,
      native_id,
      workdir,
      logDir,
    })
  } else if (status === RUNNING) {
    await updatePipeline(pipeline, RUNNING);
  } else if (['started', 'error', 'completed'].indexOf(event) > -1 ) {
    Object.assign(doc, {
      task: `${event}|${runId}`,
      ...payload,
      [event]: Math.floor(Date.parse(utcTime) / 1000)
    })
  } else return true // We're not interested in other events (like submitted, running etc)

  try {
    await docClient.put({
      TableName: PROGRESS_TABLE,
      Item: doc
    }).promise()
    return true
  } catch (err) {
    console.log({ error: `Couldn't update progress of ${pipeline}` })
    return false
  }
}

function getLogDir(pipeline) {
  if (NEXTFLOW_LOG_DIR === undefined || !NEXTFLOW_LOG_DIR.startsWith('s3://')) throw Error(`NEXTFLOW_LOG_DIR should be like 's3://bucket/path' not ${NEXTFLOW_LOG_DIR}`);
  const dir = new URL(`${NEXTFLOW_LOG_DIR}/${pipeline}`)
  dir.pathname = dir.pathname.replace(/\/+/g, '/')
  return dir.href;
}

function slugify(tag) {
  return tag.replace(/[^a-zA-Z0-9.\-]+/g, '_').replace(/_+/g, '_').replace(/^_+/, '').replace(/_+$/, '')
}

async function backupLogs({ workdir, pipeline, process, status, tag, attempt }) {
  // The workdir has a lifecycle run; files are deleted after 30 days
  // This backs up the log files
  // workdir looks like "s3://${FilesBucket}/workDir/${pipeline}/3c/5ef18730ad06f5af5fffb2e196cdac"
  const { protocol, pathname, hostname: fromBucket } = new URL(workdir)
  if (protocol !== 's3:') return console.log(`Error: ${workdir} doesn't appear to be an s3 directory`)
  const workdirPrefix = pathname.slice(1) + (workdir.endsWith('/') ? "" : "/")

  const logDir = getLogDir(slugify(pipeline));
  const { pathname: toPathname, hostname: ToBucket } = new URL(logDir)
  const logDirPrefix = toPathname.slice(1) + (toPathname.endsWith('/') ? "" : "/") + slugify(process) + '/' + slugify(`${status}.${process}.${tag}.${attempt}`)

  const logFiles = [];
  await forObjects({ Bucket: fromBucket, Prefix: workdirPrefix }, keys => {
    for (const fromKey of keys) {
      const fname = fromKey.split('/').slice(-1)[0]
      if (fname.startsWith('.command') || fname === '.exitcode') {
        const toKey = logDirPrefix + '/' + slugify(fname)
        logFiles.push({ fromKey, toKey })
      }
    }
  })

  for (const { fromKey, toKey } of logFiles) {
    await s3.copyObject({ Bucket: ToBucket, Key: toKey, CopySource: `/${fromBucket}/${fromKey}` }).promise();
  }

  return logDir
}

async function updatePipeline(pipeline, status, retries=MAX_RETRIES) {
  console.log(JSON.stringify({ pipeline, status }))
  try {
    await docClient.update({
      TableName: PIPELINES_TABLE,
      Key: { pipeline },
      UpdateExpression: "set #status = :status, #time = :time",
      ConditionExpression: '#status <> :success AND #status <> :status',
      ExpressionAttributeValues: {
        ':status': status,
        ':success': SUCCESS,
        ':time': Math.floor(new Date().getTime() / 1000)
      },
      ExpressionAttributeNames: {
        "#status": "status",
        "#time": `timestamp:${status}`
      }
    }).promise()
    return true
  } catch (err) {
    console.log({ retries, retry: "updatePipeline", pipeline })
    if (retries <= 0) return false
    if (err.code && err.code === 'ConditionalCheckFailedException') return true // Pipeline was already successfull
    if (err.code && err.code === 'TransactionConflictException') {
      await jitter(200)
      return updatePipeline(pipeline, status, retries-1)
    }
    return false
  }
}

async function handleWebUpdate(event) {
  const { error, code, pipeline, sourceIp, payload } = validate(event);
  if (error !== undefined) return res(400, { error, code })

  await appendProgress(pipeline, sourceIp, payload)
  return res(200, { pipeline })
}

async function handleECSUpdate(event) {
  const { detail={} } = event;
  const { clusterArn, containers=[], overrides={} } = detail;
  if (clusterArn !== NEXTFLOW_CLUSTER_ARN) return console.log(`Ignoring ECS events from ${clusterArn}, only ${NEXTFLOW_CLUSTER_ARN} is supported`);
  const container = containers[0] || {};
  const { name, exitCode } = container;
  if (name !== "nextflow") return console.log(`Error: expected a container called 'nextflow', got ${name}`)
  if (exitCode === undefined) return; // Container hasn't finished

  const { containerOverrides=[] } = overrides;
  const { name: overridesName, environment=[] } = containerOverrides[0] || {};
  if (overridesName !== "nextflow") return console.log(`Error: expected overrides for a container called 'nextflow', got ${overridesName}`)
  const { value: pipeline } = environment.filter(_ => _.name === '__NF_RUNNER_PIPELINE')[0] || {};
  if (pipeline === undefined) return console.log(`Error: couldn't parse name of pipeline`)

  if (exitCode === 0) return await updatePipeline(pipeline, SUCCESS)
  else return await updatePipeline(pipeline, FAILED)
}

exports.handler = async function(event) {
  if (!environmentVariablesOk()) return
  if (event.source === 'aws.ecs') return await handleECSUpdate(event);
  return await handleWebUpdate(event)
}
